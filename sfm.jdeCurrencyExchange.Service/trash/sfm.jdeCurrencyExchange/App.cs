﻿using Microsoft.Extensions.Configuration;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace sfm.jdeCurrencyExchange
{
    public static class App
    {
        public static IConfiguration Configuration
        {
            get
            {
                var path = Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName;
                var baseDirectory = "C:\\source\\repo\\sfm.jdeCurrencyExchange\\sfm.jdeCurrencyExchange\\sfm.jdeCurrencyExchange\\sfm.jdeCurrencyExchange";
                var result = new ConfigurationBuilder()
               .SetBasePath(baseDirectory)
               //.SetBasePath(path)
               .AddJsonFile("appsettings.json", optional: true)
               .Build();
                return result;
            }
        }

        private static StandardKernel _kernel;

        /// <summary>
        /// Ninject Kernal used for creating objects
        /// </summary>
        public static IKernel Kernel
        {
            get
            {
                if (_kernel == null)
                {
                    _kernel = new StandardKernel();
                    _kernel.Load(Assembly.GetExecutingAssembly());
                }
                return _kernel;
            }
        }
    }
}