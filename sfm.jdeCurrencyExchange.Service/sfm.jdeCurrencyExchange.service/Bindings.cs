﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ninject;
using Ninject.Modules;
using Sfm.CurrencyExchangeService;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using Sfm.CurrencyExchangeService.Persistence;
using Sfm.CurrencyExchangeService.Services;
using Sfm.CurrencyExchangeService.Settings;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sfm.jdeCurrencyExchange
{
    public class Bindings : NinjectModule
    {
        private EmailSettings emailSettings = App.Configuration.GetSection("EmailSettings").Get<EmailSettings>();
        private ErrorSettings errorSettings = App.Configuration.GetSection("ErrorSettings").Get<ErrorSettings>();
        private CurrencySettings currencySettings = App.Configuration.GetSection("CurrencySettings").Get<CurrencySettings>();
        private BankOfCanadaSettings bankOfCanadaSettings = App.Configuration.GetSection("BankOfCanadaSettings").Get<BankOfCanadaSettings>();
        private JdeConnectionSettings jdeConnectionSettings = App.Configuration.GetSection("JdeConnectionSettings").Get<JdeConnectionSettings>();

        public override void Load()
        {
            Bind<EmailSettings>()
              .ToConstant(emailSettings);

            Bind<CurrencySettings>()
                .ToConstant(currencySettings);

            Bind<BankOfCanadaSettings>()
                .ToConstant(bankOfCanadaSettings);

            Bind<ErrorSettings>()
              .ToConstant(errorSettings);

            Bind<JdeConnectionSettings>()
                .ToConstant(jdeConnectionSettings);

            Bind<ExchangeRateController>()
                    .To<ExchangeRateController>();

            Bind<IExchangeRateServiceSource>()
                .To<ExchangeRateServiceSource>();

            Bind<IEmailSender>()
                .To<EmailSender>();

            Bind<IErrorNotifier>()
              .To<ErrorNotifier>().InSingletonScope();

            Bind<IERPRepository>()
                .To<ERPRepository>();

            Bind<IExchangeRateServiceDestination>()
                .To<ExchangeRateServiceDestination>();

            Bind<IDataSource>().
                To<DataSource>();

            Bind<IHttpJsonClient>().
               To<HttpJsonClient>();
            // Bind<IValueResolver<SourceEntity, DestModel, bool>>().To<MyResolver>();

            var mapperConfiguration = CreateConfiguration();
            Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();

            // This teaches Ninject how to create automapper instances say if for instance
            // MyResolver has a constructor with a parameter that needs to be injected
            Bind<IMapper>().ToMethod(ctx =>
                 new Mapper(mapperConfiguration, type => ctx.Kernel.Get(type)));
        }

        private MapperConfiguration CreateConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // Add all profiles in current assembly
                cfg.AddProfiles(GetType().Assembly);
            });

            return config;
        }
    }
}