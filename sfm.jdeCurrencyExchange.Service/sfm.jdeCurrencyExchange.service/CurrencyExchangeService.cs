﻿using Microsoft.Extensions.Logging;
using Sfm.CurrencyExchangeService;
using System;
using System.ServiceProcess;
using System.Timers;
using Ninject;
using Sfm.CurrencyExchangeService.Helpers;
using Sfm.CurrencyExchangeService.Contants;

namespace sfm.jdeCurrencyExchange.service
{
    public partial class CurrencyExchangeService : ServiceBase
    {
        private Timer _timer = null;
        private double _pollInterval = 0;
        private int _timeHourOfService = 0;
        private int _noOfMinutesToVerifyValidation = 0;

        public CurrencyExchangeService()
        {
            InitializeComponent();
            Extensions.Iteration = 5;
            var windowsServiceSettings = App.Configuration.GetSection("WindowsServiceSettings");
            _pollInterval = Convert.ToDouble(windowsServiceSettings["PollInterval"]);
            _timeHourOfService = Convert.ToInt32(windowsServiceSettings["TimeHourOfService"]);
            _noOfMinutesToVerifyValidation = Convert.ToInt32(windowsServiceSettings["noOfMinutesToVerifyValidation"]);
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            //Console.WriteLine();
            //Console.WriteLine("Onstart");

            //set service validation flags
            ServiceValidation.SetValid();
            ServiceValidation.SetEmailNotSent();
            ServiceValidation.SetNotUpdatedCurrency();
            //initialize logger
            ILoggerFactory loggerFactory = new LoggerFactory();
            AppLogging.Initialize(loggerFactory, App.Configuration);

            AppLogging.LogDebug("Export field service started.");

            //Console.WriteLine("Time @" + DateTime.Now.Hour.ToString("00.##") + ":" + DateTime.Now.Minute.ToString("00.##"));
            _timer = new Timer(_pollInterval);
            _timer.Elapsed += ProcessCurrencyExchange;
            _timer.Enabled = true;
            _timer.AutoReset = false;  // makes it fire only once
            _timer.Start();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            AppLogging.LogDebug("Service stopped.");
        }

        private void ProcessCurrencyExchange(object sender, EventArgs e)
        {
            //Console.WriteLine("Start of ProcessCurrencyExchange @" + DateTime.Now.Hour.ToString("00.##") + ":" + DateTime.Now.Minute.ToString("00.##"));
            AppLogging.LogDebug("Processing iteration :" + Extensions.Iteration);
            //if (Extensions.Iteration < 0)
            //{
            //    throw new Exception("Uh oh!");
            //}
            //Extensions.Iteration--;
            try
            {
                if (TimeToRun() && ServiceValidation.CurrencyUpdated == CurrencyExchangeServiceType.NOTUPDATEDCURRENCY)
                {
                    SetServiceValid();
                    ILoggerFactory loggerFactory = new LoggerFactory();
                    AppLogging.Initialize(loggerFactory, App.Configuration);
                    AppLogging.LogDebug("Log date: " + DateTime.Now);
                    ExchangeRateController exchangeRate = App.Kernel.Get<ExchangeRateController>();
                    exchangeRate.UpdateRates();
                    //Console.WriteLine("Updated Currency Exchange.");
                }
                if (!TimeToRun()) ServiceValidation.SetNotUpdatedCurrency();
            }
            catch (Exception ex)
            {
                AppLogging.LogError(ex, "General Error. System Stopped Working.");
            }
            finally
            {
                //Console.WriteLine("End of ProcessCurrencyExchange.");
                _timer.Start();
            }
        }

        private void SetServiceValid()
        {
            int minutesTime = DateTime.Now.Minute;
            int flagToSetValidaiton = minutesTime % _noOfMinutesToVerifyValidation;
            if (flagToSetValidaiton == 0 && ServiceValidation.SentEmail == CurrencyExchangeServiceType.NOTSENT)
            {
                ServiceValidation.SetValid();
                ServiceValidation.SetEmailSent();
            }
            if (flagToSetValidaiton == 1)
            {
                ServiceValidation.SetEmailNotSent();
            }
        }

        private bool TimeToRun()
        {
            bool result = false;
            DayOfWeek day = DateTime.Now.DayOfWeek;
            string dayToday = day.ToString();
            result = DateTime.Now.Hour == _timeHourOfService && dayToday != DayOfWeek.Saturday.ToString() && dayToday != DayOfWeek.Sunday.ToString();
            return result;
        }
    }
}