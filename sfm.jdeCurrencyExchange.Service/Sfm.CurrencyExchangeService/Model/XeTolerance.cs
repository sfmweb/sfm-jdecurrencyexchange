﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Model
{
    public class CurrencyTolerance
    {
        public string CurrencyFrom { get; set; }
        public string CurrencyTo { get; set; }
        public double Tolerance { get; set; }
        public string Type { get; set; }
    }
}