﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Model
{
    public class ExchangeRateList
    {
        public ExchangeRateList()
        {
            List = new List<ExchangeRate>();
        }

        public List<ExchangeRate> List { get; set; }
    }
}