﻿using System;

namespace Sfm.CurrencyExchangeService.Model
{
    public class ExchangeRate
    {
        public string CurrencyCodeFrom { get; set; }
        public string CurrencyCodeTo { get; set; }
        public DateTime DateEffective { get; set; }
        public double CurrencyConversionRateSpotRate { get; set; }
        public double CurrencyConversionRateDivisor { get; set; }
    }
}