﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Contants
{
    public class CurrencyExchangeServiceType
    {
        public const string VALID = "true";
        public const string INVALID = "false";
        public const string SENT = "sent";
        public const string NOTSENT = "notsent";
        public const string UPDATEDCURRENCY = "updated";
        public const string NOTUPDATEDCURRENCY = "notupdated";
    }
}