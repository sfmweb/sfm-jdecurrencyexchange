﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfm.CurrencyExchangeService.Contants;

namespace Sfm.CurrencyExchangeService
{
    public static class ServiceValidation
    {
        static ServiceValidation()
        {
            Status = CurrencyExchangeServiceType.VALID;
        }

        public static string Status { get; private set; }

        public static string SentEmail { get; private set; }

        public static string CurrencyUpdated { get; private set; }

        public static void SetInvalid()
        {
            Status = CurrencyExchangeServiceType.INVALID;
        }

        public static void SetValid()
        {
            Status = CurrencyExchangeServiceType.VALID;
        }

        public static void SetEmailSent()
        {
            SentEmail = CurrencyExchangeServiceType.SENT;
        }

        public static void SetEmailNotSent()
        {
            SentEmail = CurrencyExchangeServiceType.NOTSENT;
        }

        public static void SetUpdatedCurrency()
        {
            CurrencyUpdated = CurrencyExchangeServiceType.UPDATEDCURRENCY;
        }

        public static void SetNotUpdatedCurrency()
        {
            CurrencyUpdated = CurrencyExchangeServiceType.NOTUPDATEDCURRENCY;
        }
    }
}