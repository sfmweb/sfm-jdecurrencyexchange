﻿using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly string _host;
        private readonly string _from;
        private readonly int _port;
        private readonly string _username;
        private readonly string _password;
        private readonly bool _enableSsl;

        public EmailSender(EmailSettings emailSetting)
        {
            _host = emailSetting.Host;
            _from = emailSetting.From;
            _port = emailSetting.Port;
            _username = emailSetting.Username;
            _password = emailSetting.Password;
            _enableSsl = emailSetting.EnableSsl;
        }

        public bool SendEmail(string emailFrom, IEnumerable<EmailAddress> emailsTo, IEnumerable<EmailAddress> emailsCcs, IEnumerable<EmailAddress> emailsBccs, string subject, string body, bool isHtml, EmailPriority priority, IEnumerable<EmailAttachment> attachments)
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(emailFrom);
            foreach (var emaiTo in emailsTo)
            {
                mail.To.Add(emaiTo.Email);
            }

            //cc
            if (emailsCcs != null)
            {
                foreach (var emailsCc in emailsCcs)
                {
                    mail.CC.Add(emailsCc.Email);
                }
            }
            //bcc
            if (emailsBccs != null)
            {
                foreach (var emailsBcc in emailsBccs)
                {
                    mail.Bcc.Add(emailsBcc.Email);
                }
            }

            //add attachments
            if (attachments != null)
            {
                Attachment attachment;
                foreach (var attachmentFile in attachments)
                {
                    attachment = new Attachment(attachmentFile.GetAttachmentUrl());
                    mail.Attachments.Add(attachment);
                }
            }

            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = isHtml;
            mail.Priority = MapPriority(priority);
            using (SmtpClient smtp = new SmtpClient(_host, _port))
            {
                smtp.Credentials = new NetworkCredential(_username, _password);
                smtp.EnableSsl = _enableSsl;
                try
                {
                    smtp.Send(mail);
                }
                catch (SmtpException ex)
                {
                    AppLogging.LogError(ex, "Unable to send email");
                }
            }
            return true;
        }

        public bool SendEmail(string emailTo, string subject, string body, EmailPriority priority)
        {
            EmailAddress emailAddressTo = new EmailAddress(emailTo, emailTo);
            List<EmailAddress> emailList = new List<EmailAddress>();
            emailList.Add(emailAddressTo);
            SendEmail(_from, emailList, null, null, subject, body, false, priority, null);
            return true;
        }

        private MailPriority MapPriority(EmailPriority priority)
        {
            MailPriority mappedPriority = MailPriority.Normal;
            if (priority == EmailPriority.High)
                mappedPriority = MailPriority.High;
            if (priority == EmailPriority.Normal)
                mappedPriority = MailPriority.Normal;
            if (priority == EmailPriority.Low)
                mappedPriority = MailPriority.Low;

            return mappedPriority;
        }
    }
}