﻿namespace Sfm.CurrencyExchangeService.Services
{
    public class EmailAttachment
    {
        public EmailAttachment(string name, string path)
        {
            Name = name;
            Path = path;
        }

        public string Name { get; private set; }
        public string Path { get; private set; }

        public string GetAttachmentUrl()
        {
            return Path + "/" + Name;
        }
    }
}