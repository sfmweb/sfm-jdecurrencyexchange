﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Services
{
    public class EmailAddress
    {
        public EmailAddress(string email, string displayName)
        {
            Email = email;
            DisplayName = displayName;
        }

        public string Email { get; set; }
        public string DisplayName { get; set; }
    }
}