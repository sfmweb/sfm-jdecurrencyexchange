﻿using AutoMapper;
using Sfm.CurrencyExchangeService.Exceptions;
using Sfm.CurrencyExchangeService.Helpers;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using Sfm.CurrencyExchangeService.Persistence;
using Sfm.CurrencyExchangeService.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Services
{
    public class ERPRepository : IERPRepository
    {
        private readonly IDataSource _dataSource;
        private readonly CurrencySettings _currencySettings;
        private JdeConnectionSettings _settings;
        private readonly IMapper _mapper;

        public ERPRepository(JdeConnectionSettings settings,
            IDataSource dataSource,
            CurrencySettings currencySettings,
            IMapper mapper
            )
        {
            _mapper = mapper;
            _dataSource = dataSource;
            _currencySettings = currencySettings;
            _settings = settings;
        }

        public ExchangeRateList GetCurrentRates()
        {
            DbConnection connection = _dataSource.CreateDbConnection();

            ExchangeRateList exchangeRateList = new ExchangeRateList();
            foreach (var currencySetting in _currencySettings.CurrencyToleranceSettings)
            {
                string currencyTo = currencySetting.CurrencyExchange.Substring(0, 3).ToUpper();
                string currencyFrom = currencySetting.CurrencyExchange.Substring(3, 3).ToUpper();

                exchangeRateList.List.Add(GetExchageRateForCurrencyFromAndTo(currencyFrom, currencyTo));
            }
            _dataSource.CloseDbConnection();
            return exchangeRateList;
        }

        public void SaveRates(ExchangeRateList filteredRates)
        {
            DbConnection connection = _dataSource.CreateDbConnection();
            foreach (ExchangeRate exchangeRate in filteredRates.List)
            {
                if (_settings.SetToOneToWriteToDb == 1)
                {
                    InsertFromToAndToFromInErp(exchangeRate);
                }
            }
            _dataSource.CloseDbConnection();
        }

        private void InsertFromToAndToFromInErp(ExchangeRate exchangeRate)
        {
            ExchangeRate ToFromexchangeRate = MappExchahgeRateToMirror(exchangeRate);
            InsertInErp(exchangeRate);
            InsertInErp(ToFromexchangeRate);
        }

        private ExchangeRate MappExchahgeRateToMirror(ExchangeRate exchangeRate)
        {
            ExchangeRate ToFromexchangeRate = new ExchangeRate();
            ToFromexchangeRate.CurrencyCodeFrom = exchangeRate.CurrencyCodeTo;
            ToFromexchangeRate.CurrencyCodeTo = exchangeRate.CurrencyCodeFrom;
            ToFromexchangeRate.CurrencyConversionRateDivisor = exchangeRate.CurrencyConversionRateSpotRate;
            ToFromexchangeRate.CurrencyConversionRateSpotRate = exchangeRate.CurrencyConversionRateDivisor;
            ToFromexchangeRate.DateEffective = exchangeRate.DateEffective;
            return ToFromexchangeRate;
        }

        private void InsertInErp(ExchangeRate exchangeRate)
        {
            DbCommand command = BuildCommandInsert(exchangeRate);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (DbException e)
            {
                _dataSource.CloseDbConnection();
                AppLogging.LogError(e, "Error excecuting sql command: " + command.ToLogOutput());
                throw new DataAccessException("Error excecuting sql command: " + command.ToLogOutput(), e);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                    command = null;
                }
            }
        }

        private DbCommand BuildCommandInsert(ExchangeRate item)
        {
            DbCommand command = _dataSource.CreateCommand();

            command.AddParameter("@currencyCodeFrom", item.CurrencyCodeFrom, DbType.String);
            command.AddParameter("@currencyCodeTo", item.CurrencyCodeTo, DbType.String);
            command.AddParameter("@dateEffective", item.DateEffective.ToJdeDate(), DbType.Int32);
            command.AddParameter("@currencyConversionRateSpotRate", item.CurrencyConversionRateSpotRate, DbType.Double);
            command.AddParameter("@currencyConversionRateDivisor", item.CurrencyConversionRateDivisor, DbType.Double);
            command.AddParameter("@dateUpdated", DateTime.Now.ToJdeDate(), DbType.Int32);
            command.AddParameter("@timeLastUpdated", DateTime.Now.ToJdeTime(), DbType.Int32);

            string insertQuery = $@"insert into   {_settings.DataLibrary}.F0015
                (CXCRCD,CXCRDC,CXAN8,CXRTTYP,CXEFT,CXCLMETH,CXCRCM,CXTRCR,CXCRR,CXCRRD,CXCSR,CXUSER,CXPID,CXJOBN,CXUPMJ,CXUPMT)
                values
           (@currencyCodeFrom,@currencyCodeTo,0,'',@dateEffective,1,'Y','',@currencyConversionRateSpotRate,@currencyConversionRateDivisor,1,'SYSSVCS','P0015A','JDE9JASSV0',@dateUpdated,@timeLastUpdated)";

            command.CommandText = insertQuery;
            return command;
        }

        private DbCommand BuildCommandGetCurrentRates(string currencyFrom, string currencyTo)
        {
            DbCommand command = _dataSource.CreateCommand();
            string selectQuery = $@"SELECT * FROM {_settings.DataLibrary}.F0015 WHERE CXCRDC = '{currencyFrom}' AND CXCRCD = '{currencyTo}' ORDER BY CXEFT DESC FETCH FIRST 1 ROWS ONLY";

            command.CommandText = selectQuery;
            return command;
        }

        private ExchangeRate GetExchageRateForCurrencyFromAndTo(string currencyFrom, string currencyTo)
        {
            DbCommand command = BuildCommandGetCurrentRates(currencyFrom, currencyTo);
            ExchangeRate exchangeRate = new ExchangeRate();
            try
            {
                DbDataReader ERPCurrency = command.ExecuteReader();
                var dt = new DataTable();
                dt.Load(ERPCurrency);
                List<DataRow> dr = dt.AsEnumerable().ToList();
                ERPCurrency.Close();
                exchangeRate = MapERPCurrencyToExchangeRate(dr.FirstOrDefault());
            }
            catch (DbException e)
            {
                _dataSource.CloseDbConnection();
                AppLogging.LogError(e, "Error excecuting sql command: " + command.ToLogOutput());
                throw new DataAccessException("Error excecuting sql command: " + command.ToLogOutput(), e);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                    command = null;
                }
            }
            return exchangeRate;
        }

        private ExchangeRate MapERPCurrencyToExchangeRate(DataRow dataRow)
        {
            ExchangeRate exchangeRate = new ExchangeRate();
            exchangeRate.CurrencyCodeFrom = dataRow[0].ToString();
            exchangeRate.CurrencyCodeTo = dataRow[1].ToString();
            exchangeRate.DateEffective = (Convert.ToInt32(dataRow[4])).FromJdeDate();
            exchangeRate.CurrencyConversionRateSpotRate = Convert.ToDouble(dataRow[8].ToString());
            exchangeRate.CurrencyConversionRateDivisor = Convert.ToDouble(dataRow[9]);

            return exchangeRate;
        }
    }
}