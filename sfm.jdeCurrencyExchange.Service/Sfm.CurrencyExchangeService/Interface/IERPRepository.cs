﻿using Sfm.CurrencyExchangeService.Model;

namespace Sfm.CurrencyExchangeService.Interface
{
    public interface IERPRepository
    {
        ExchangeRateList GetCurrentRates();

        void SaveRates(ExchangeRateList filteredRates);
    }
}