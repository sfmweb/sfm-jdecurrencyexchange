﻿using Sfm.CurrencyExchangeService.Model;

namespace Sfm.CurrencyExchangeService.Interface
{
    public interface IExchangeRateServiceDestination
    {
        void ProcessRates(ExchangeRateList currencyRates);
    }
}