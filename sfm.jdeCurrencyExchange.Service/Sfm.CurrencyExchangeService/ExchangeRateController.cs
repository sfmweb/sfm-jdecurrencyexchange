﻿using Microsoft.Extensions.Configuration;
using Sfm.CurrencyExchangeService.Exceptions;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Sfm.CurrencyExchangeService.Contants;

namespace Sfm.CurrencyExchangeService
{
    public class ExchangeRateController
    {
        private readonly IExchangeRateServiceSource _exchangeRateServiceSource;
        private readonly IExchangeRateServiceDestination _exchangeRateServiceDestination;
        private readonly IErrorNotifier _errorNotifier;

        public ExchangeRateController(IExchangeRateServiceSource exchangeRateServiceSource,
            IExchangeRateServiceDestination exchangeRateServiceDestination,
            IErrorNotifier errorNotifier
            )
        {
            _errorNotifier = errorNotifier;
            _exchangeRateServiceDestination = exchangeRateServiceDestination;
            _exchangeRateServiceSource = exchangeRateServiceSource;
        }

        public void UpdateRates()
        {
            try
            {
                ExchangeRateList currencyRates = _exchangeRateServiceSource.GetRates();
                _exchangeRateServiceDestination.ProcessRates(currencyRates);
                ServiceValidation.SetUpdatedCurrency();
                if (ServiceValidation.Status == CurrencyExchangeServiceType.INVALID)
                {
                    ServiceValidation.SetValid();
                    _errorNotifier.Notify("Successful Update of Currency Exchange", "The currency exchange was updated successfully on " + DateTime.Now + ".");
                    ServiceValidation.SetEmailSent();
                }
            }
            catch (HttpJsonException e)
            {
                if (ServiceValidation.Status == CurrencyExchangeServiceType.VALID)
                {
                    _errorNotifier.Notify("Currency Exchange - Http CLient Error", "Not able to retrieve the exchange rate.", e);
                    ServiceValidation.SetEmailSent();
                }
                ServiceValidation.SetInvalid();
            }
            catch (DataAccessException e)
            {
                if (ServiceValidation.Status == CurrencyExchangeServiceType.VALID)
                {
                    _errorNotifier.Notify("Currency Exchange - Database Error", "Not able to connect to database.", e);
                    ServiceValidation.SetEmailSent();
                }
                ServiceValidation.SetInvalid();
            }
        }
    }
}