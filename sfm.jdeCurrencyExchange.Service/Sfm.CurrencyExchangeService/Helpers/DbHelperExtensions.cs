﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Helpers
{
    public static class DbHelperExtensions
    {
        public static void AddParameter(this IDbCommand command, string name, object value, DbType dbType)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = name;
            parameter.Value = value;
            parameter.DbType = dbType;
            command.Parameters.Add(parameter);
        }

        public static void AddParameterFromColumn(this IDbCommand command, string name, string column, DbType dbType)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = name;
            parameter.SourceColumn = column;
            parameter.DbType = dbType;
            command.Parameters.Add(parameter);
        }

        public static string ToLogOutput(this IDbCommand command)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(command.CommandText);
            foreach (IDbDataParameter parameter in command.Parameters)
                sb.Replace(parameter.ParameterName, parameter.IsQuoted() + (parameter.Value != null ? parameter.Value.ToString() : "") + parameter.IsQuoted());

            string output = sb.ToString();

            output = Regex.Replace(output, @"\s+", " ");
            output.Trim();

            return sb.ToString();
        }

        private static string IsQuoted(this IDbDataParameter parameter)
        {
            string quote = "";
            switch (parameter.DbType)
            {
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                case DbType.Binary:
                case DbType.String:
                case DbType.StringFixedLength:
                case DbType.Date:
                case DbType.DateTime:
                case DbType.DateTime2:
                case DbType.DateTimeOffset:
                case DbType.Xml:
                    quote = "'";
                    break;

                default:
                    quote = "";
                    break;
            }

            return quote;
        }

        /// <summary>
        /// Converts a datetime object into a jde julian date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int ToJdeDate(this DateTime date)
        {
            if (date == null || date == DateTime.MinValue)
                return 0;

            int jdeDate = 0;
            int dayInYear = date.DayOfYear;
            int theYear = date.Year - 1900;
            jdeDate = (theYear * 1000) + dayInYear;

            return jdeDate;
        }

        /// <summary>
        /// Converts a jde julian date into a datetime object
        /// </summary>
        /// <param name="julianDate"></param>
        /// <returns></returns>
        public static DateTime FromJdeDate(this int julianDate)
        {
            if (julianDate == 0)
                return DateTime.MinValue;

            int realJulian = julianDate + 1900000;
            int year = Convert.ToInt32(realJulian.ToString().Substring(0, 4));
            int dayOfYear = Convert.ToInt32(realJulian.ToString().Substring(4));

            DateTime jdeDate = new DateTime(year, 1, 1).AddDays(dayOfYear - 1);

            return jdeDate;
        }

        public static int ToJdeTime()
        {
            string time = DateTime.Now.ToString("HHmmss");
            int.TryParse(time, out int jdeTime);

            return jdeTime;
        }

        public static int ToJdeTime(this DateTime date)
        {
            string time = date.ToString("HHmmss");
            int.TryParse(time, out int jdeTime);

            return jdeTime;
        }

        public static string Clean(this string stringToClean)
        {
            return stringToClean
                .Trim()
                .Replace("\u001a", string.Empty);
        }
    }
}