﻿namespace Sfm.CurrencyExchangeService.Settings
{
    public class ErrorSettings
    {
        public bool SendErrors { get; set; } = true;
        public string ErrorEmailTo { get; set; }
    }
}