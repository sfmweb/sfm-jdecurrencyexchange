﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Settings
{
    public class JdeConnectionSettings
    {
        public string ConnectionString { get; set; }
        public string Provider { get; set; }
        public string DataLibrary { get; set; }
        public string ControlLibrary { get; set; }
        public int SetToOneToWriteToDb { get; set; }
    }
}