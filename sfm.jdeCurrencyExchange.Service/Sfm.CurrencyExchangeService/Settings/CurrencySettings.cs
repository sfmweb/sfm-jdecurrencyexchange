﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Settings
{
    public class CurrencySettings
    {
        [JsonProperty("CurrencyToleranceSettings")]
        public CurrencyToleranceSetting[] CurrencyToleranceSettings { get; set; }
    }

    public class CurrencyToleranceSetting
    {
        [JsonProperty("CurrencyExchange")]
        public string CurrencyExchange { get; set; }

        [JsonProperty("Tolerance")]
        public double Tolerance { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }
    }
}