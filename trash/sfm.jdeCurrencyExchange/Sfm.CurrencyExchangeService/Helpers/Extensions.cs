﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Helpers
{
    public static class Extensions
    {
        private static string IsQuoted(this IDbDataParameter parameter)
        {
            string quote = "";
            switch (parameter.DbType)
            {
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                case DbType.Binary:
                case DbType.String:
                case DbType.StringFixedLength:
                case DbType.Date:
                case DbType.DateTime:
                case DbType.DateTime2:
                case DbType.DateTimeOffset:
                case DbType.Xml:
                    quote = "'";
                    break;

                default:
                    quote = "";
                    break;
            }

            return quote;
        }

        public static bool CheckNullOrEmpty<T>(this T value)
        {
            /// <summary>
            /// return true if it is null or empty
            /// </summary>
            /// verify string
            if (typeof(T) == typeof(string))
            {
                if (!string.IsNullOrEmpty(value as string))
                {
                    return string.IsNullOrEmpty(value.ToString().Trim());
                }
                else
                {
                    return true;
                }
            }
            // verify object
            else if (typeof(T) == typeof(object))
            {
                if (value != null)
                {
                    return string.IsNullOrEmpty(value.ToString().Trim());
                }
            }
            // verify primitive type
            return value == null || value.Equals(default(T));
        }

        public static bool Notify { get; set; }

        public static int EmailsSent { get; set; }

        //for testing
        public static void WriteToFile(this string Str, string Filename)
        {
            File.WriteAllText(Filename, Str);
            return;
        }
    }
}