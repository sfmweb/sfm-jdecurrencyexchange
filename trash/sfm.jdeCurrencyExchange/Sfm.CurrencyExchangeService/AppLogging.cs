﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService
{
    public class AppLogging
    {
        private static ILoggerFactory _factory = null;
        private static Microsoft.Extensions.Logging.ILogger _logger = null;

        /// <summary>
        /// Initializes the logger, should be called only once in the application startup
        /// </summary>
        /// <param name="factory">The ILogger Factory to use for logging</param>
        /// <param name="configuration">The configuration file to read logging settings from</param>
        public static void Initialize(ILoggerFactory factory, IConfiguration configuration)
        {
            _factory = factory;

            Serilog.ILogger logger = new LoggerConfiguration()
              .ReadFrom.Configuration(configuration)
              //.MinimumLevel.Debug()
              //.WriteTo.RollingFile("C:\\Test\\Log\\log-{Date}.json")
              .CreateLogger();
            _factory.AddSerilog(logger);

            _logger = _factory.CreateLogger("Logger");
        }

        public static void LogTrace(string message, params object[] args) => _logger?.LogTrace(message, args);

        //public static void LogTrace(EventId eventId, string message, params object[] args) => _logger?.LogTrace(eventId, message, args);
        public static void LogTrace(Exception exception, string message, params object[] args) => _logger?.LogTrace(exception, message, args);

        //public static void LogTrace(EventId eventId, Exception exception, string message, params object[] args) => _logger?.LogTrace(eventId, exception, message, args);

        public static void LogDebug(string message, params object[] args) => _logger?.LogDebug(message, args);

        //public static void LogDebug(EventId eventId, string message, params object[] args) => _logger?.LogDebug(eventId, message, args);
        public static void LogDebug(Exception exception, string message, params object[] args) => _logger?.LogDebug(exception, message, args);

        //public static void LogDebug(EventId eventId, Exception exception, string message, params object[] args) => _logger?.LogDebug(eventId, exception, message, args);

        public static void LogInformation(string message, params object[] args) => _logger?.LogInformation(message, args);

        //public static void LogInformation(EventId eventId, string message, params object[] args) => _logger?.LogInformation(eventId, message, args);
        public static void LogInformation(Exception exception, string message, params object[] args) => _logger?.LogInformation(exception, message, args);

        //public static void LogInformation(EventId eventId, Exception exception, string message, params object[] args) => _logger?.LogInformation(eventId, exception, message, args);

        public static void LogWarning(string message, params object[] args) => _logger?.LogWarning(message, args);

        //public static void LogWarning(EventId eventId, string message, params object[] args) => _logger?.LogWarning(eventId, message, args);
        public static void LogWarning(Exception exception, string message, params object[] args) => _logger?.LogWarning(exception, message, args);

        //public static void LogWarning(EventId eventId, Exception exception, string message, params object[] args) => _logger?.LogWarning(eventId, exception, message, args);

        public static void LogError(string message, params object[] args) => _logger?.LogError(message, args);

        //public static void LogError(EventId eventId, string message, params object[] args) => _logger?.LogError(eventId, message, args);
        public static void LogError(Exception exception, string message, params object[] args) => _logger?.LogError(exception, message, args);

        //public static void LogError(EventId eventId, Exception exception, string message, params object[] args) => _logger?.LogError(eventId, exception, message, args);

        public static void LogCritical(string message, params object[] args) => _logger?.LogCritical(message, args);

        //public static void LogCritical(EventId eventId, string message, params object[] args) => _logger?.LogCritical(eventId, message, args);
        public static void LogCritical(Exception exception, string message, params object[] args) => _logger?.LogCritical(exception, message, args);

        //public static void LogCritical(EventId eventId, Exception exception, string message, params object[] args) => _logger?.LogCritical(eventId, exception, message, args);
    }
}