﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Exceptions
{
    public class HttpJsonException : Exception
    {
        public HttpJsonException()
        {
        }

        public HttpJsonException(string message) : base(message)
        {
        }

        public HttpJsonException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}