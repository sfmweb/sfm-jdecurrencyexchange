﻿using AutoMapper;
using Sfm.CurrencyExchangeService.Contants;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using Sfm.CurrencyExchangeService.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Services
{
    public class XEService : IXEService
    {
        private readonly IERPRepository _eRPRepository;
        private readonly CurrencySettings _currencySettings;
        private readonly IMapper _mapper;

        public XEService(IERPRepository eRPRepository,
            CurrencySettings currencySettings,
            IMapper mapper
            )
        {
            _mapper = mapper;
            _currencySettings = currencySettings;
            _eRPRepository = eRPRepository;
        }

        public void ProcessRates(ExchangeRateList currencyRates)
        {
            ExchangeRateList eRPRates = _eRPRepository.GetCurrentRates();
            ExchangeRateList filteredRates = FilterRates(eRPRates, currencyRates);
            _eRPRepository.SaveRates(filteredRates);
        }

        private ExchangeRateList FilterRates(ExchangeRateList eRPRates, ExchangeRateList currencyRates)
        {
            ExchangeRateList filterRates = new ExchangeRateList();
            foreach (ExchangeRate exchangeRate in currencyRates.List)
            {
                var eRPRate = eRPRates.List.FirstOrDefault(t => t.CurrencyCodeFrom.Equals(exchangeRate.CurrencyCodeFrom));
                CurrencyTolerance toleranceCurrency = getToleranceSetting(exchangeRate);
                ExchangeRate filteredExchageRate = SelectExchangeRate(toleranceCurrency, eRPRate, exchangeRate);
                if (filteredExchageRate.CurrencyCodeFrom != null)
                    filterRates.List.Add(filteredExchageRate);
            }

            return filterRates;
        }

        private ExchangeRate SelectExchangeRate(CurrencyTolerance toleranceCurrency, ExchangeRate eRPRate, ExchangeRate exchangeRate)
        {
            ExchangeRate filteredExchageRate = new ExchangeRate();

            if (toleranceCurrency.Type.Equals(ToleranceType.PERCENT))
                filteredExchageRate = selectExchangeRateBasedOnPercentage(toleranceCurrency, eRPRate, exchangeRate);

            if (toleranceCurrency.Type.Equals(ToleranceType.POINT))
                filteredExchageRate = selectExchangeRateBasedOnPoint(toleranceCurrency, eRPRate, exchangeRate);

            return filteredExchageRate;
        }

        private ExchangeRate selectExchangeRateBasedOnPoint(CurrencyTolerance toleranceCurrency, ExchangeRate eRPRate, ExchangeRate newExchangeRate)
        {
            ExchangeRate filteredExchageRate = new ExchangeRate();
            double change = Math.Abs(eRPRate.CurrencyConversionRateSpotRate - newExchangeRate.CurrencyConversionRateSpotRate) * Math.Pow(10, 5);
            if (change > toleranceCurrency.Tolerance)
                filteredExchageRate = MapExchangeRateToFilteredExchangeRate(newExchangeRate);

            return filteredExchageRate;
        }

        private ExchangeRate selectExchangeRateBasedOnPercentage(CurrencyTolerance toleranceCurrency, ExchangeRate eRPRate, ExchangeRate newExchangeRate)
        {
            ExchangeRate filteredExchageRate = new ExchangeRate();
            double change = Math.Abs((eRPRate.CurrencyConversionRateSpotRate - newExchangeRate.CurrencyConversionRateSpotRate) * 100 / eRPRate.CurrencyConversionRateSpotRate);
            if (change > toleranceCurrency.Tolerance)
                filteredExchageRate = MapExchangeRateToFilteredExchangeRate(newExchangeRate);
            return filteredExchageRate;
        }

        private ExchangeRate MapExchangeRateToFilteredExchangeRate(ExchangeRate exchangeRate)
        {
            ExchangeRate filteredExchageRate = _mapper.Map<ExchangeRate>(exchangeRate);
            return filteredExchageRate;
        }

        private CurrencyTolerance getToleranceSetting(ExchangeRate exchangeRate)
        {
            CurrencyTolerance tolerance = new CurrencyTolerance();
            List<CurrencyTolerance> currencyToleranceList = GetToleranceList();

            tolerance.CurrencyFrom = exchangeRate.CurrencyCodeFrom;
            tolerance.CurrencyTo = exchangeRate.CurrencyCodeTo;
            tolerance.Tolerance = currencyToleranceList.FirstOrDefault(t => t.CurrencyFrom.Equals(tolerance.CurrencyFrom)).Tolerance;
            tolerance.Type = currencyToleranceList.FirstOrDefault(t => t.CurrencyFrom.Equals(tolerance.CurrencyFrom)).Type;
            return tolerance;
        }

        private string CurrencyCodeFrom(string curXe)
        {
            return curXe.Substring(2, 3).ToUpper();
        }

        private string CurrencyCodeTo(string curXe)
        {
            return curXe.Substring(5, 3).ToUpper();
        }

        private List<CurrencyTolerance> GetToleranceList()
        {
            List<CurrencyTolerance> currencyToleranceList = new List<CurrencyTolerance>();
            foreach (var currencyTolerance in _currencySettings.CurrencyToleranceSettings)
            {
                CurrencyTolerance xeTolerance = new CurrencyTolerance();
                xeTolerance.CurrencyFrom = currencyTolerance.CurrencyExchange.Substring(0, 3).ToUpper();
                xeTolerance.CurrencyTo = currencyTolerance.CurrencyExchange.Substring(3, 3).ToUpper();
                xeTolerance.Tolerance = currencyTolerance.Tolerance;
                xeTolerance.Type = currencyTolerance.Type;
                currencyToleranceList.Add(xeTolerance);
            }
            return currencyToleranceList;
        }
    }
}