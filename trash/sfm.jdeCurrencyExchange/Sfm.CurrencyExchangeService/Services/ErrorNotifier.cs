﻿using Sfm.CurrencyExchangeService.Interface;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Services
{
    public class ErrorNotifier : IErrorNotifier
    {
        private readonly IEmailSender _emailSender;
        private readonly ErrorSettings _errorSettings;

        public ErrorNotifier(IEmailSender emailSender, ErrorSettings errorSettings)
        {
            _emailSender = emailSender;
            _errorSettings = errorSettings;
        }

        public void Notify(string title, string message, Exception ex)
        {
            if (_errorSettings.SendErrors)
            {
                string body = string.Format("A processing error has occured.\nError: {0}\nException: {1}", message, ex);

                body += "\n";
                body += "Data:\n";
                foreach (DictionaryEntry data in ex.Data)
                {
                    body += $"{data.Key}:{data.Value}";
                }

                _emailSender.SendEmail(_errorSettings.ErrorEmailTo, title, body, EmailPriority.High);
            }
        }

        public void Notify(string title, string message)
        {
            if (_errorSettings.SendErrors)
            {
                string body = message;

                _emailSender.SendEmail(_errorSettings.ErrorEmailTo, title, body, EmailPriority.High);
            }
        }
    }
}