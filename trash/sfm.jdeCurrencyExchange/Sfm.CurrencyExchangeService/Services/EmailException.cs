﻿using System;
using System.Runtime.Serialization;

namespace Sfm.CurrencyExchangeService.Services
{
    [Serializable]
    internal class EmailException : Exception
    {
        public EmailException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}