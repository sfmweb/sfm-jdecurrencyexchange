﻿namespace Sfm.CurrencyExchangeService.Services
{
    public class ErrorSettings
    {
        public bool SendErrors { get; set; } = true;
        public string ErrorEmailTo { get; set; }
    }
}