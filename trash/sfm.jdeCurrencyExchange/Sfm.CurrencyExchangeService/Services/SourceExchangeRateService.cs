﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using Sfm.CurrencyExchangeService.Settings;

namespace Sfm.CurrencyExchangeService.Services
{
    public class ExchangeRateServiceSource : IExchangeRateServiceSource
    {
        private readonly CurrencySettings _currencySettings;
        private readonly BankOfCanadaSettings _bankOfCanadaSettings;
        private readonly IHttpJsonClient _httpJsonClient;

        public ExchangeRateServiceSource(
            CurrencySettings currencySettings,
            BankOfCanadaSettings bankOfCanadaSettings,
            IHttpJsonClient httpJsonClient)
        {
            _currencySettings = currencySettings;
            _bankOfCanadaSettings = bankOfCanadaSettings;
            _httpJsonClient = httpJsonClient;
        }

        public ExchangeRateList GetRates()
        {
            return MapRatesToModel(GetBankOfCanadaRates());
        }

        private BankOfCanadaExchangeRates GetBankOfCanadaRates()
        {
            BankOfCanadaExchangeRates result = new BankOfCanadaExchangeRates();

            string startDay = DateTime.Now.AddDays(-_bankOfCanadaSettings.DaysInterval).ToString("yyyy-MM-dd");
            string url = _bankOfCanadaSettings.Url + startDay;

            string json = _httpJsonClient.GetStringAsync(url);
            result = JsonConvert.DeserializeObject<BankOfCanadaExchangeRates>(json, Converter.Settings);

            return result;
        }

        private ExchangeRateList MapRatesToModel(BankOfCanadaExchangeRates boc)
        {
            Observation lastObservation = boc.Observations.ToList().OrderByDescending(t => t.D).FirstOrDefault();
            ExchangeRateList xeList = new ExchangeRateList();

            foreach (var currency in _currencySettings.CurrencyToleranceSettings)
            {
                string bankOfCanadaCurrency = "Fx" + currency.CurrencyExchange.ToLower();

                PropertyInfo currencyOservation = typeof(Observation).GetProperties().ToList().Find(c => c.Name == bankOfCanadaCurrency);

                if (currencyOservation != null)
                {
                    ExchangeRate xerateSpotRate = MapToExchangeRateValue(lastObservation, currencyOservation);
                    xeList.List.Add(xerateSpotRate);
                }
            }
            return xeList;
        }

        private ExchangeRate MapToExchangeRateValue(Observation lastObservation, PropertyInfo currencyOservation)
        {
            ExchangeRateValue value = currencyOservation.GetValue(lastObservation) as ExchangeRateValue;

            double valueSpotToRate = value.Value;

            double valueDivisory = 1 / valueSpotToRate;

            ExchangeRate xerateSpotRate = new ExchangeRate();
            xerateSpotRate.CurrencyCodeFrom = CurrencyCodeFrom(currencyOservation.Name);
            xerateSpotRate.CurrencyCodeTo = CurrencyCodeTo(currencyOservation.Name);
            xerateSpotRate.CurrencyConversionRateSpotRate = valueSpotToRate;
            xerateSpotRate.CurrencyConversionRateDivisor = valueDivisory;
            xerateSpotRate.DateEffective = DateTime.Now;

            return xerateSpotRate;
        }

        private string CurrencyCodeFrom(string currObsName)
        {
            return currObsName.Substring(2, 3).ToUpper();
        }

        private string CurrencyCodeTo(string currObsName)
        {
            return currObsName.Substring(5, 3).ToUpper();
        }
    }
}