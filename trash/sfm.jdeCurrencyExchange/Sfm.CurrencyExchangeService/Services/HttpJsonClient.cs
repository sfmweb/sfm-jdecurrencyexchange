﻿using Newtonsoft.Json;
using Sfm.CurrencyExchangeService.Exceptions;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Services
{
    public class HttpJsonClient : IHttpJsonClient
    {
        private HttpClient _client;
        private readonly IErrorNotifier _errorNotifier;

        public HttpJsonClient(
            IErrorNotifier errorNotifier)
        {
            _errorNotifier = errorNotifier;
            _client = new HttpClient();
        }

        public string GetStringAsync(string url)
        {
            using (_client)
            {
                string json = "";
                // Call asynchronous network methods in a try/catch block to handle exceptions
                try
                {
                    // Above three lines can be replaced with new helper method below
                    json = _client.GetStringAsync(url).Result;
                }
                catch (AggregateException ae)
                {
                    foreach (var e in ae.InnerExceptions)
                    {
                        // Handle the custom exception.
                        if (e is HttpRequestException)
                        {
                            AppLogging.LogError(e, "Error Http Client {url}", url);
                            throw new HttpJsonException("Error Http Client" + url, e);
                        }
                        // Rethrow any other exception.
                        else
                        {
                            AppLogging.LogError(e, "General Error at HttpJsonClient level {url}", url);
                            throw new HttpJsonException("General Error at HttpJsonClient level" + url, e);
                        }
                    }
                }
                return json;
            }
        }
    }
}