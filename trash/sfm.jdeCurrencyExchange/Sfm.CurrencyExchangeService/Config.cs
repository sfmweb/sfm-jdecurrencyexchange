﻿using Microsoft.Extensions.Configuration;
using Sfm.CurrencyExchangeService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService
{
    public static class Config
    {
        public static IConfiguration Configuration
        {
            get
            {
                string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"bin\" }, StringSplitOptions.None)[0];

                return new ConfigurationBuilder()
               .SetBasePath(projectPath)
               .AddJsonFile("appsettings.json", optional: true)
               .AddEnvironmentVariables()
               .Build();
            }
        }

        public static IConfigurationSection JdeConnectionSettings => Configuration.GetSection("JdeConnectionSettings");
        private static IConfigurationSection ServiceSettings => Configuration.GetSection("BankOfCanadaSettings");
        private static IConfigurationSection EmailSettings => Configuration.GetSection("EmailSettings");
        private static IConfigurationSection ErrorSettings => Configuration.GetSection("ErrorSettings");

        public static CurrencyType[] Currencies => Configuration.GetSection("Currencies").Get<CurrencyType[]>();

        public static string JdeConnectionString => JdeConnectionSettings["ConnectionString"];
        public static string JdeDbProviderName => JdeConnectionSettings["Provider"];
        public static string JdeDataLibrary => JdeConnectionSettings["DataLibrary"];
        public static string JdeControlLibrary => JdeConnectionSettings["ControlLibrary"];

        public static int StockMarketDaysInterval => Convert.ToInt32(ServiceSettings["DaysInterval"]);

        public static string EmailHost => EmailSettings["Host"];
        public static int EmailPort => Convert.ToInt32(EmailSettings["Port"]);
        public static string EmailUserName => EmailSettings["UserName"];
        public static string EmailPassword => EmailSettings["Password"];
        public static bool EmailEnableSql => Convert.ToBoolean(EmailSettings["EnableSSL"]);
        public static string EmailFrom => EmailSettings["From"];

        public static string ErrorEmailTo => ErrorSettings["SendEmailTo"];
        public static bool EmailSendErrorEmails => Convert.ToBoolean(ErrorSettings["SendErrorEmail"]);
    }
}