﻿using Microsoft.Extensions.Configuration;
using Sfm.CurrencyExchangeService.Exceptions;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService
{
    public class ExchangeRateController
    {
        private readonly IExchangeRateServiceSource _exchangeRateService;
        private readonly IXEService _xEService;
        private readonly IErrorNotifier _errorNotifier;

        public ExchangeRateController(IExchangeRateServiceSource exchangeRateService,
            IXEService xEService,
            IErrorNotifier errorNotifier
            )
        {
            _errorNotifier = errorNotifier;
            _xEService = xEService;
            _exchangeRateService = exchangeRateService;
        }

        public void UpdateRates()
        {
            try
            {
                ExchangeRateList currencyRates = _exchangeRateService.GetRates();
                _xEService.ProcessRates(currencyRates);
            }
            catch (HttpJsonException e)
            {
                _errorNotifier.Notify("Http CLient Error", "Not able to retrieve the exchange rate.", e);
            }
        }
    }
}