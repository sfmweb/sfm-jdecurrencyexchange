﻿using Sfm.CurrencyExchangeService.Model;

namespace Sfm.CurrencyExchangeService.Interface
{
    public interface IExchangeRateServiceSource
    {
        ExchangeRateList GetRates();
    }
}