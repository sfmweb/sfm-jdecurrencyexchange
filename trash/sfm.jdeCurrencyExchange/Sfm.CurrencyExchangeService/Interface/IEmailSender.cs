﻿using Sfm.CurrencyExchangeService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Interface
{
    public interface IEmailSender
    {
        bool SendEmail(string emailTo, string subject, string body, EmailPriority priority);
    }
}