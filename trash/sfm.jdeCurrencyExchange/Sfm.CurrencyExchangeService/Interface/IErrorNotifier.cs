﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Interface
{
    public interface IErrorNotifier
    {
        void Notify(string title, string message, Exception ex);

        void Notify(string title, string message);
    }
}