﻿using Sfm.CurrencyExchangeService.Model;

namespace Sfm.CurrencyExchangeService.Interface
{
    public interface IXEService
    {
        void ProcessRates(ExchangeRateList currencyRates);
    }
}