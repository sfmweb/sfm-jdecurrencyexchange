﻿using Sfm.CurrencyExchangeService.Model;

namespace Sfm.CurrencyExchangeService.Interface
{
    public interface IHttpJsonClient
    {
        string GetStringAsync(string url);
    }
}