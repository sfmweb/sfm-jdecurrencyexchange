﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Contants
{
    public class ToleranceType
    {
        public const string PERCENT = "Percent";
        public const string POINT = "Point";
    }
}