﻿using Microsoft.Extensions.Configuration;
using Sfm.CurrencyExchangeService.Exceptions;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Persistence
{
    public class DataSource : IDataSource, IDisposable
    {
        private DbConnection _connection;
        protected JdeConnectionSettings _settings;
        protected DbProviderFactory _providerFactory;

        public DataSource(JdeConnectionSettings settings,

            IErrorNotifier errorNotifier)
        {
            _settings = settings;
            _providerFactory = ProviderFactory(_settings);
        }

        private DbProviderFactory ProviderFactory(JdeConnectionSettings settings)
        {
            try
            {
                return DbProviderFactories.GetFactory(settings.Provider);
            }
            catch (ArgumentException e)
            {
                AppLogging.LogError(e, "Unable to find the requested .Net Framework Data Provider: " + settings.Provider);
                throw new DataAccessException("Unable to find the requested .Net Framework Data Provider: " + settings.Provider, e);
            }
        }

        public DbConnection CreateDbConnection()
        {
            AppLogging.LogDebug("Opening DB connection.");
            if (_connection == null)
            {
                // Create the DbProviderFactory and DbConnection.
                if (_settings.ConnectionString != null)
                {
                    try
                    {
                        _connection = _providerFactory.CreateConnection();
                        _connection.ConnectionString = _settings.ConnectionString;
                        _connection.Open();
                    }
                    catch (DbException ex)
                    {
                        CloseDbConnection();
                        AppLogging.LogError(ex, "Error opening DB connection {connection}", _connection);
                        throw new DataAccessException("Cannot connect to database", ex);
                    }
                }
            }
            AppLogging.LogDebug("Connection to DB opened {connection}", _connection);
            return _connection;
        }

        public DbCommand CreateCommand()
        {
            return _connection.CreateCommand();
        }

        public void CloseDbConnection()
        {
            if (_connection != null)
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();

                _connection.Dispose();
                _connection = null;
            }
        }

        private bool disposedValue = false;

        public void Dispose()
        {
            if (!disposedValue)
            {
                _connection.Dispose();
                disposedValue = true;
            }
        }
    }
}