﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Persistence
{
    public interface IDataSource
    {
        DbConnection CreateDbConnection();

        DbCommand CreateCommand();

        void CloseDbConnection();

        void Dispose();
    }
}