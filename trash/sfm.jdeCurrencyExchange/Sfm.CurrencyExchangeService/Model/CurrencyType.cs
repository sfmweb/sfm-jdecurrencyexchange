﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Model
{
    public class CurrencyType
    {
        public string CurrencyExchange { get; set; }
        public int Tolerance { get; set; }
        public string Type { get; set; }
    }
}