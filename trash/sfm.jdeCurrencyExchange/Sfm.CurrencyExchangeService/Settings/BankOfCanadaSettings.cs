﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sfm.CurrencyExchangeService.Settings
{
    public class BankOfCanadaSettings
    {
        [JsonProperty("DaysInterval")]
        public int DaysInterval { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}