﻿using Microsoft.Extensions.Logging;
using Ninject;
using Sfm.CurrencyExchangeService;
using Sfm.CurrencyExchangeService.Interface;
using Sfm.CurrencyExchangeService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sfm.jdeCurrencyExchange
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ILoggerFactory loggerFactory = new LoggerFactory();
            AppLogging.Initialize(loggerFactory, App.Configuration);
            AppLogging.LogDebug("Log date: " + DateTime.Now);

            try
            {
                ExchangeRateController exchangeRate = App.Kernel.Get<ExchangeRateController>();
                exchangeRate.UpdateRates();
            }
            catch (Exception e)
            {
                AppLogging.LogError(e, "General Error. System Stopped Working.");
            }
        }
    }
}